export class GridDetails{
    public x: number = 0;
    public y: number = 4;
    public id: string = "";
    public isActive: boolean = false;
    public activeDirection: string = "";
}