import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'toy-robot-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ToyRobot';

  checkEnv: string = environment.text;

  constructor(){
    console.log(environment.grid);
  }
}
