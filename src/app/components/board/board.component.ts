import { Component, OnInit } from "@angular/core";
import { direction } from "src/app/constants/directions.constant";
import { GridDetails } from "src/app/model/grid-details.model";
import { environment } from "src/environments/environment";

@Component({
    selector: 'toy-robot-board',
    templateUrl: './board.component.html',
    styleUrls: ['./board.component.scss']
  })

  export class BoardComponent implements OnInit{
    logs = new Array<string>();
    id: string ="";
    gridCount: number = environment.grid.x * environment.grid.y;

    arrGrid = new Array();
    selectedDirection: string = "south";
    selectedCommand: string = "";
    activeDirection: string = "south"
    directions = direction;

    constructor(){}
    ngOnInit(): void {
      this.selectedCommand = "place";
      this.initializeGrid();
      console.log(this.arrGrid);
    }

    private initializeGrid(){
      this.arrGrid = new Array();
      for (let index = 0; index < 5; index++) {
        for (let idx = 5; idx > 0; idx--) {
          let grid = new GridDetails();
          grid.x = index
          grid.y = (idx-1);
          grid.id = index.toString() +"," +(idx-1).toString();
          this.arrGrid.push(grid);
        }
      }
      this.id = "";
      this.selectedDirection = "south";
      this.selectedCommand = "place";
    }

    private faceRight(grid:GridDetails){
      if(grid.activeDirection == "north"){
        grid.activeDirection = "east";
      } else if(grid.activeDirection == "east") {
        grid.activeDirection = "south";
      } else if( grid.activeDirection == "south"){
        grid.activeDirection="west";
      } else if(grid.activeDirection == "west"){
        grid.activeDirection = "north";
      }
      this.appendLogs("RIGHT");
    }

    private faceLeft(grid: GridDetails){
      if(grid.activeDirection == "north"){
        grid.activeDirection = "west";
      } else if(grid.activeDirection == "west") {
        grid.activeDirection = "south";
      } else if( grid.activeDirection == "south"){
        grid.activeDirection="east";
      } else if(grid.activeDirection == "east"){
        grid.activeDirection = "north";
      }
      this.appendLogs("LEFT");
    }
    private leftRight(grid: GridDetails){
      if(this.selectedCommand == "right"){
        this.faceRight(grid);
      } else{
        this.faceLeft(grid);
      }
      console.log(grid.id);
    }

    execute(id: string){


      let grid = this.arrGrid.find(grd=>grd.id == this.id)
      if(this.selectedCommand == "left" || this.selectedCommand == "right"){
        this.leftRight(grid);
        console.log('leftright')
      }
      
      if(this.selectedCommand == "place"){
        let grid = this.arrGrid.find(grd=>grd.id == this.id)
        if(this.arrGrid.findIndex(grid=> grid.isActive) != -1){
          this.arrGrid.find(before=>before.isActive).isActive = false;
          
          
        }
        if(this.arrGrid.find(grd=>grd.id == this.id)){
          
          grid.isActive = true;
          grid.activeDirection = this.selectedDirection
        } else {
          alert ("OUT OF BOUNDS");
        }
        this.appendLogs(this.selectedCommand.toUpperCase() + " " + this.id + " " + grid.activeDirection.toUpperCase());
        console.log(this.arrGrid);
      } else if(this.selectedCommand == "move"){
        if(this.arrGrid.findIndex(grid=> grid.isActive) == -1){
          alert("EXECUTE PLACE COMMAND FIRST");
        } else {
          console.log("MOVE");
          if(this.arrGrid.find(grd=>grd.isActive) ){
            let activeGrid:GridDetails = this.arrGrid.find(s=>s.isActive);
            this.appendLogs(this.selectedCommand.toUpperCase());
            switch(activeGrid.activeDirection){
              case "north":
                if(this.arrGrid.findIndex(s=> (s.y == activeGrid.y+1 && s.x == activeGrid.x))!= -1){
                  let find = this.arrGrid.find(findGrid =>(findGrid.y == activeGrid.y+1 && findGrid.x == activeGrid.x));
                  this.changeStatus(activeGrid, find);
                  console.log("north",this.arrGrid);
                }else {
                  alert("OUT OF BOUNDS");
                }
                break;
              case "west":
                if(this.arrGrid.findIndex(s=> (s.x == activeGrid.x-1 && s.y == activeGrid.y))!= -1){
                  let find = this.arrGrid.find(findGrid =>(findGrid.x == activeGrid.x-1 && findGrid.y == activeGrid.y));
                  this.changeStatus(activeGrid, find);
                }else {
                  alert("OUT OF BOUNDS");
                }
              break;
              case "east":
                if(this.arrGrid.findIndex(s=> (s.x == activeGrid.x+1 && s.y == activeGrid.y))!= -1){
                  let find = this.arrGrid.find(findGrid =>(findGrid.x == activeGrid.x+1 && findGrid.y == activeGrid.y));
                  this.changeStatus(activeGrid, find);
                }else {
                  alert("OUT OF BOUNDS");
                }
              break;
              case "south":
                if(this.arrGrid.findIndex(s=> (s.y == activeGrid.y-1 && s.x == activeGrid.x))!= -1){
                  let find = this.arrGrid.find(findGrid =>(findGrid.y == activeGrid.y-1 && findGrid.x == activeGrid.x));
                  this.changeStatus(activeGrid, find);
                }else {
                  alert("OUT OF BOUNDS");
                }
                break;
            }
          }
        }

      }

      // this.activeDirection = this.selectedDirection;
    }

    resetAll(){
      this.initializeGrid();
    }

    private changeStatus(current: GridDetails, next: GridDetails){
      next.isActive = true;
      current.isActive = false;
      next.activeDirection = current.activeDirection;
    }

    private appendLogs(logs: string){
      this.logs.push(logs);
      this.logs.forEach(log=>{
        console.log(log)
      })
    }

  }
  