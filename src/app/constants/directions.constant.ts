export const direction = [
    {value: "north", label: "NORTH" },
    {value: "south", label: "SOUTH" },
    {value: "west", label: "WEST" },
    {value: "east", label: "EAST" },
]